## Cash Machine
### The Solution

The solution is built using a node(express) backend and a react frontend(create-react-app)

if request is made throught the frontend, an async request is made to the backend. If there request is successfull, a list is rendered with the available not, otherwise an simple error message is rendered. Input on frontend is restricted to numbers using input type attribute to minimize errors. Calls can be made directly to the the api

Solution can be previewed at http://cashcow.vasnte.com

API endpoint: http://cashcow.vasnte.com/api/withdraw/200

To run:

1. Clone repository
2. $ npm install && cd client && npm install
3. move to root folder
4. $ npm run dev

On your browser:

localhost:3000/

localhost:3001/api/withdraw/50
