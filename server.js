const express = require('express');
// var router = express.Router();
const path = require('path');

const teller = require('./services/teller');

const app = express();
const port = process.env.PORT || 3001;

app.use(express.static(path.join(__dirname, 'client/build')));

app.get('/api/withdraw/:amount', (req, res) => {
  let result = teller(req.params.amount);
  res.json({ data: result });
});

app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname + '/client/build/index.html'));
});

app.listen(port, () => console.log(`Listening on port ${port}`));
