import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Layout from './container/Layout';
import Search from './components/Search';

class App extends React.Component {
  render() {
    return (
      <Layout>
        <Search />
      </Layout>
    );
  }
}

ReactDOM.render(<App />, document.getElementById('root'));
