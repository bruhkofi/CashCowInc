import React, { Component } from 'react'
import './Layout.css'

class Layout extends Component {
  render() {
    return (
        <div className="App">
        <header className="App-header">
          {/*  <img src={logo} className="App-logo" alt="logo" /> */}
          <h1 className="App-title">Welcome to CashCow</h1>
          {this.props.children}
        </header>
        </div>
    )
  }
}
export default Layout
