import React, { Component } from 'react';
import './Search.css';

class Search extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: '',
      notes: [],
      hasError: false,
      error: []
    };
  }

  handleChange = event => {
    this.setState({ value: event.target.value });
  };

  handleSubmit = event => {
    this.setState({ notes: [], hasError: false });
    event.preventDefault();
    this.contactTeller();
  };

  async contactTeller() {
    try {
      const response = await fetch(`/api/withdraw/${this.state.value}`);
      const responseData = await response.json();
      this.setState({ notes: responseData.data });
    } catch (error) {
      this.setState({ hasError: true, error });
    }
  }

  render() {
    const { notes } = this.state;
    return (
      <div>
        <p className="App-intro">Please Enter An Amount!</p>
        <form onSubmit={this.handleSubmit} className="Form">
          <label>
            <input
              className="input"
              type="number"
              value={this.state.value}
              onChange={this.handleChange}
            />
          </label>
          <input type="submit" value="Submit" className="submitButton" />
        </form>

        <ul className="liste">
          {notes.map((note, i) => <li key={i}>${note}</li>)}
        </ul>
        {this.state.hasError ? <div>OOps</div> : null}
      </div>
    );
  }
}

export default Search;
