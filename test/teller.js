var expect = require('chai').expect
var assert = require('chai').assert;
const teller = require('../services/teller');


const requestedAmount = 180;
describe('Teller', () => {
  describe('invalid params', () => {
    it('should throw a NoteUnavailableException if there are no denominations available for the requested amount', () => {
      expect(() => teller(requestedAmount + 5))
        .to.throw(Error, "NoteUnavailableException");
    });

    it('should throw an InvalidArgumentException if input is a negative number', () => {
      expect(() => teller(-requestedAmount))
        .to.throw(Error, "InvalidArgumentException");
    });

    it('should return an empty array if null is passed', () => {
      expect((teller(null))).to.deep.equal([]);
    });

  });

});



