const teller = request => {
  var availableNotes = [100, 50, 20, 10],
    withdrawal = [];

  if (request < 0) {
    throw new Error('InvalidArgumentException');
  }

  if (request % availableNotes[3]) {
    throw new Error('NoteUnavailableException');
  }
  const getNotes = () => {
    while (request > 0) {
      for (var i = 0; i < availableNotes.length; i++) {
        var note = availableNotes[i];
        if (request - note >= 0) {
          request -= note;
          withdrawal.push(note);
          break;
        }
      }
    }
  };

  if (request && request > 0) {
    getNotes();
  }

  return withdrawal;
};

module.exports = teller;
